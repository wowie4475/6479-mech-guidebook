# Welcome to 6479's Mech Handbook

Whether you're a complete novice or more experienced, this handbook will cover the basic foundations of what you'll encounter as a member of our mechanical subteam

## Handbook Overview

### [Introduction](0-introduction/index.md)

Welcome!

### [Measuring, Clamping, & Marking](01-measuring-clamping-marking/index.md)

This section will cover measu

### [Stock & Materials](02-stock-materials/index.md)

Lorem Ipsum

### [Tools](03-tools/index.md)

Lorem Ipsum

### [Larger Machinery](04-machinery/index.md)

Lorem Ipsum

### [Important Principles](05-principles/index.md)

Lorem Ipsum

### [Hardware & Fasteners](06-hardware-fasteners/index.md)

Lorem Ipsum

### [Wheels & Flywheels](07-wheels-flywheels/index.md)

Lorem Ipsum

### [Energy Transfer Mechanisms](08-energy-transfer/index.md)

Lorem Ipsum

### [Motors & Gearboxes](09-motors-gearboxes/index.md)

Lorem Ipsum

### [Robot Mechanisms](10-robot-mechanisms/index.md)

Lorem Ipsum